#include "Control.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;

// -------------------------------------------------------------------------
Glib::RefPtr<Control> Control::create(const std::string &name, const std::string &prefix) {
    auto conf = uniset_conf();
    ObjectId id = conf->getObjectID(name);

    if (id == DefaultObjectId)
        throw SystemError("(Control::init): Unknown ID for '" + name + "'");

    shared_ptr<UniXML> xml = conf->getConfXML();
    xmlNode *cnode = xml->findNode(xml->getFirstNode(), "Control", name);

    if (cnode == NULL)
        throw SystemError("(Control): Not found confnode for name='" + name + "'");

    Control *ctl = new Control(id, cnode, prefix);
    auto act = UniSetActivator::Instance();
    //act->addObject(static_cast<UniSetObject*>(ctl));
    act->add(shared_ptr<UniSetObject>(ctl));
    return Glib::RefPtr<Control>(ctl);
}

// -------------------------------------------------------------------------
Control::Control(uniset::ObjectId id, xmlNode *cnode, const std::string &prefix) :
        UObjectT<Control_SK>(id, cnode, prefix),
        btnRPM(0),
        setPower1(0),
        btnUBSheme(0),
        build(0),
        ubuild(0) {
    uconf = UserConf::Instance();
    dlgPass = PasswordDialog::Instance();
    Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();

    UniXML::iterator it(cnode);

    dlgSetValue = SetValueDialog::Instance();
    dlgSetValue->show_btnMinus();
    cmdTimeout = it.getPIntProp("cmdTimeout", 3000);

    //Обработчик нажатия кнопки Здадание по W
    GETWIDGET(btnVSetting, "ctl_btn_w");
    btnVSetting->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnVSetting_clicked));

    //Обработчик нажатия кнопки Здадание по P
    GETWIDGET(btnPSetting, "ctl_btn_P");
    btnPSetting->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnPSetting_clicked));

    //Обработчик октрытия выставление задачи оборотов
    GETWIDGET(btnRPM, "ctl_btn_RPM");
    btnRPM->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnSetRPM_clicked));

    //Обработчик нажатия кнопки Задание мощности ПЧ1
    GETWIDGET(setPower1, "ctl_btn_FQC1");
    setPower1->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnsetPower1_clicked));

    /*//Обработчик нажатия кнопки Задание мощности ПЧ2
    GETWIDGET(setPower2, "ctl_btn_FQC2");
    setPower2->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnsetPower2_clicked));*/

    //Обработчик нажатия кнопки Собрать схему
    GETWIDGET(btnBSheme, "ctl_btn_BSheme");
    btnBSheme->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnBuildScheme_clicked));

    //Обработчик нажатия кнопки Разобрать схему
    GETWIDGET(btnUBSheme, "ctl_btn_UBSheme");
    btnUBSheme->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnUBuildScheme_clicked));

    //Обработчик нажатия кнопки ПУСК
    GETWIDGET(btnPusk, "ctl_btn_Pusk");
    btnPusk->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnPusk_clicked));

    //Обработчик нажатия кнопки СТОП
    GETWIDGET(btnStop, "ctl_btn_Stop");
    btnStop->signal_clicked().connect(sigc::mem_fun(*this, &Control::btnStop_clicked));

    GETWIDGET(mainw, "fixed1");

    GETWIDGET(lbl, "lbl_zadan");
    GETWIDGET(setFQC, "SetFQC");

    qdialog = new Gtk::MessageDialog(("Связь с управляющим контроллером отсуствует..."), false, Gtk::MESSAGE_INFO,
            Gtk::BUTTONS_NONE, true);
    qdialog->set_default_response(Gtk::RESPONSE_NO);
    qdialog->set_position(Gtk::WIN_POS_CENTER);
    qdialog->set_keep_above(true);

    /* по-умолчанию блокируем задание оборотов и мощности*/
    blockcontrol(false);

    /* по-умолчанию режим задания по оборотам*/

    vset();
    out_SettingMode = fcmSetRPM;
}

// -------------------------------------------------------------------------
Control::~Control() {
    delete qdialog;
}

// -------------------------------------------------------------------------
void Control::step() {
 out_ERR=0;
	// в цикле проверяем, что ПЧ подтвердил команду на сборку схемы и сбрасываем
    if (in_IBuildSheme == true) {
        out_BuildSheme = false;

    }
    if (in_IUnBuildSheme ==true) {
        out_UnBuildSheme = false;
    }


    //блокируем управление если переключатели находятся в дистанции и если переключатель находится в положении
    //задатчик
    if (!in_LocalSVU && !in_ControlScreen)
    {
        blockcontrol(true);
        //при переводе с дистанции подхватываем управлени с СВУ
    } else{
        //управление заблокировано
        blockcontrol(false);

    }


    if (in_LocalSVU  || in_ControlScreen)
    {
        //если управление от СВУ
        out_VSettingTemp = in_VSettingZAD;
        out_PSettingTemp = in_PSettingSVU;
        out_VSettingScreen = in_VSettingZAD;
        out_PSettingScreen = in_PSettingSVU;
        cout << "out_VSettingScreen=" << out_VSettingScreen<< endl;
    }

    //если пропала связь предупреждаем об этом пользователя и блокируем полностью экран
    if (in_CANExchange) {


        mainw->set_sensitive(true);
        qdialog->hide();
    } else {

        mainw->set_sensitive(false);
        qdialog->show();
	out_ERR=2; //выставляем 2 бит в единицу, для отображения сообщени в журнале
   }

if( in_GUI)
{
	out_ERR=1;
}
 }

// -------------------------------------------------------------------------
void Control::blockcontrol(bool par) {
    btnUBSheme->set_sensitive(par);
    btnBSheme->set_sensitive(par);
    btnPusk->set_sensitive(par);
    btnStop->set_sensitive(par);
    setPower1->set_sensitive(par);
    btnRPM->set_sensitive(par);
    btnVSetting->set_sensitive(par);
    btnPSetting->set_sensitive(par);
    //cout << "par=" << par << "StateScheme=" << in_StateSheme << endl;
    //если управление разблокировано и мы знаем состояние схемы то блокируем/разблокируем кнопки Сбора и Разбора
    //схемы в соотвествии с текущим состоянием схемы
    if (par) {
        if (in_StateSheme) {
            btnBSheme->set_sensitive(!par);
            btnUBSheme->set_sensitive(par);
        } else {
            btnBSheme->set_sensitive(par);
            btnUBSheme->set_sensitive(!par);

        }
    }



/*    //	btnRPM->set_sensitive(par);
    //	setPower1->set_sensitive(par);
    cout << "par=" << par << " in_ISettingMode=" << in_ISettingMode << endl;
    btnVSetting->set_sensitive(par);
    btnPSetting->set_sensitive(par);

    if (par == false) {
        btnBSheme->set_sensitive(par);
        btnUBSheme->set_sensitive(par);
        btnRPM->set_sensitive(par);
        setPower1->set_sensitive(par);
    } else {
        if (in_ShemeStatus == true) {
            btnBSheme->set_sensitive(false);
            btnUBSheme->set_sensitive(true);
            btnRPM->set_sensitive(true);
            setPower1->set_sensitive(true);
            btnVSetting->set_sensitive(true);
            btnPSetting->set_sensitive(true);
            btnPusk->set_sensitive(true);
            btnStop->set_sensitive(true);
        } else {
            btnBSheme->set_sensitive(true);
            btnUBSheme->set_sensitive(false);
            btnRPM->set_sensitive(false);
            setPower1->set_sensitive(false);
            btnVSetting->set_sensitive(false);
            btnPSetting->set_sensitive(false);
            btnPusk->set_sensitive(false);
            btnStop->set_sensitive(false);
        }
    }

*/
}

// -------------------------------------------------------------------------
void Control::mnemoset(bool set) {
    //mnemobook->set_current_page(int(set));
}

// -------------------------------------------------------------------------
// преобразование текста (по сути с игнорированием LOCALE)
// заменяет "," --> "." чтобы atof корректно сработала
static float str2F(const string &ss) {
    string s(ss);
    string::size_type pos = s.find(",");

    while (pos != string::npos) {
        s.replace(pos, 1, ".");
        pos = s.find(",");
    }

    return atof(s.c_str());
}

// -------------------------------------------------------------------------
void Control::btnVSetting_clicked() {
    vset();
}

// -------------------------------------------------------------------------
void Control::btnPSetting_clicked() {
    pset();
}

// -------------------------------------------------------------------------
void Control::pset() {
    out_SettingMode = fcmSetPower; //согласовано с ПЧ 2=режим задания по мощности
    lbl->set_label("Задание по P, кВт:");
    btnRPM->hide();
    setFQC->show();

 }

// -------------------------------------------------------------------------
void Control::vset() {
out_SettingMode = fcmSetRPM; //согласовано с ПЧ 1=режим задания по скорости
    lbl->set_label("Задание на N, об/мин:");
    btnRPM->show();
    setFQC->hide();

 }

// -------------------------------------------------------------------------
void Control::btnSetRPM_clicked() {
    //сохраняем значения введенный с клавиатуры во временный датчик
    ostringstream sval;
    sval << in_IVSettingTemp;
    cout << "sval=" << sval.str() << endl;
    string txt = dlgSetValue->run(sval.str(), "Задание оборотов, об/мин", 0, 1250);
    float val = str2F(txt);
    cout << "val=" << val << endl;
    out_VSettingTemp = val;
}

// -------------------------------------------------------------------------
void Control::btnsetPower1_clicked() {
    //сохраняем значения введенный с клавиатуры во временный датчик
    ostringstream sval;
    sval << in_IPSettingTemp;
    string txt = dlgSetValue->run(sval.str(), "Задание мощности, кВт", 0, 3000);
    float val = str2F(txt);
    cout << "val=" << val << endl;
    if (dlgSetValue->responseOK()) {
        out_PSettingTemp = val;
    }
 }

// -------------------------------------------------------------------------
void Control::btnBuildScheme_clicked() {
    out_BuildSheme = true;

 }

void Control::btnUBuildScheme_clicked() {
    out_UnBuildSheme = true;
 }
// -------------------------------------------------------------------------
void Control::btnPusk_clicked() {
    //при нажатии на пуск даем команду ПЧ на задания из временных датчиков
    out_VSettingScreen = in_IVSettingTemp;
    out_PSettingScreen = in_IPSettingTemp;

}

void Control::btnStop_clicked() {
    //кнопка СТОП обнуляем все задания и команды
    out_VSettingScreen = 0;
    out_PSettingScreen = 0;
    out_VSettingTemp = 0;
    out_PSettingTemp = 0;
 }
// -------------------------------------------------------------------------
