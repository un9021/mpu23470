#include "Setting.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
Glib::RefPtr<Setting> Setting::create( const std::string& name, const std::string& prefix )
{
    auto conf = uniset_conf();
	ObjectId id = conf->getObjectID(name);

	if( id == DefaultObjectId )
		throw SystemError("(Setting::init): Unknown ID for '" + name + "'");

    shared_ptr<UniXML> xml = conf->getConfXML();
	xmlNode* cnode = xml->findNode(xml->getFirstNode(), "Setting", name);

	if( cnode == NULL )
		throw SystemError("(Setting): Not found confnode for name='" + name + "'");

	auto act = UniSetActivator::Instance();
	return Glib::RefPtr<Setting>(new Setting(id, cnode, prefix));
}
// -------------------------------------------------------------------------
Setting::Setting( uniset::ObjectId id, xmlNode* cnode, const std::string& prefix ):
	enMenu(0),
	mainMenu(0),
	btnSetup(0),
	btnSetupExit(0)
{
	dlgPass = PasswordDialog::Instance();

	GETWIDGET(mainMenu, "mainmenu");
	GETWIDGET(enMenu, "enmenu");
	GETWIDGET(journal, "Journal");
	GETWIDGET(notebookjrn, "notebookjrn"); //Журнал

	//Обработчик открытия меню Инженерного меню
	GETWIDGET(btnSetup, "ctl_btn_Setup");
	btnSetup->signal_clicked().connect(sigc::mem_fun(*this, &Setting::btnSetup_clicked));

	//Обработчик закрытия меню Инженерного меню
	GETWIDGET(btnSetupExit, "ctl_btn_SetupExit");
	btnSetupExit->signal_clicked().connect(sigc::mem_fun(*this, &Setting::btnSetupExit_clicked));
}
// -------------------------------------------------------------------------
Setting::~Setting()
{
}
// -------------------------------------------------------------------------
void Setting::btnSetup_clicked()
{
	if(dlgPass->check("Engineer"))
	{
		notebookjrn->hide();
		mainMenu->hide();
		enMenu->show();
	}
}
// -------------------------------------------------------------------------
void Setting::btnSetupExit_clicked()
{
	enMenu->hide();
	mainMenu->show();

}
// -------------------------------------------------------------------------
