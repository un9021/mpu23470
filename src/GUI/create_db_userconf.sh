#!/bin/sh

dbname=userconf.db

[ -n "$1" ] && dbname="$1"

sqlite3 $dbname <<"_EOF_"

DROP TABLE IF EXISTS conf;
CREATE TABLE conf (
  key TEXT  PRIMARY KEY NOT NULL,
  val TEXT NOT NULL
);

_EOF_
