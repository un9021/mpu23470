#!/bin/sh
jmake
export PATH="${PATH}:./:../../libs/:../../libs/.libs/"

#ulimit -Sc 10000000
START=uniset2-start.sh

GTK2_RC_FILES=./gtkrc LC_ALL=ru_RU.UTF-8 ${START} -f ./mpu23470-gui --smemory-id SharedMemoryGUI \
	--datfile configure.xml \
	--mpu-guifile mpu23470.ui \
	--mpu-confile guiconfigure.xml \
	--pass-check-off \
	--no-beep \
	--dlog-add-levels any \
	--unideb-add-levels any \
	--skip-mb1 \
	--skip-mb2 \
	--unideb-add-levels any \
	--ulog-add-levels any \
	$*
killall get-gate-version.sh
