#!/bin/sh

MYDIR=`pwd`
ui_DIR=${MYDIR}

ui_LIST=`find ${ui_DIR} -name '*.ui'`

echo $ui_LIST

for ui_FILE in $ui_LIST ;
do
	echo "subst $ui_FILE"
	subst "s|>\(.*\)/\(.*\).svg<|>${MYDIR}/\2.svg<|g" "${ui_FILE}"
done
