#!/bin/sh

ELIST=`find -name 'e*.*' | grep -v "elinks.xml" | grep -v "err*" | sort`

for f in ${ELIST}; do
	cat $f | grep -v '<?xml'
done
