#include <iostream>
#include <list>
#include <UniXML.h>
// -------------------------------------------------------------------------
using namespace std;
using namespace uniset;
// -------------------------------------------------------------------------
string find_reg( UniXML& xml, const string name )
{
	UniXML::iterator it = xml.begin();
	it.goChildren();	

	for( ; it;  it++ )
	{
		if( it.getProp("name") == name )
			return it.getProp("id");
	}
	
	return "";
}

// -------------------------------------------------------------------------
struct NodeCompare:
	public std::binary_function<UniXML::iterator,UniXML::iterator, bool>
{
	inline bool operator()(const UniXML::iterator& lhs,
						const UniXML::iterator& rhs) const
	{ 
		return lhs.getProp(prop) < rhs.getProp(prop); 
	}

	NodeCompare( const std::string p ):prop(p){}

	const std::string prop;
};
// -------------------------------------------------------------------------

int main( int argc, char *argv[] )
{
	if( argc < 2 )
		return  1;
		
	try
	{
//		UniXML links("elinks.xml");
		UniXML xml(argv[1]);
/*		
		UniXML::iterator it(xml.begin());
//		it.setProp("code",argv[1]);
//		it.setProp("reg", find_reg(links,argv[1]));
		it.setProp("id", "");
		xml.save();
*/
	

		UniXML::iterator it0 = xml.begin();
		it0.goChildren();

		// Сортировка		
		std::list<UniXML::iterator> ilist;
		for( ; it0; it0++ )
			ilist.push_back(it0);
			
		cout << "sort list size: " << ilist.size() << endl;
		ilist.sort( NodeCompare("reg") );
		
		std::list<UniXML::iterator>::iterator it = ilist.begin();
		UniXML::iterator beg_it = (*it);
		it++;
		
		for( ; it!=ilist.end(); it++ )
			beg_it = ::xmlAddNextSibling( beg_it.getCurrent(), (*it).getCurrent() );
		
		xml.save();
	}
	catch(...)
	{
		cerr << "catch..." << endl;
	}
	
	
	return 0;
}
// -------------------------------------------------------------------------
