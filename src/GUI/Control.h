// -------------------------------------------------------------------------
#ifndef Control_H_
#define Control_H_
// -------------------------------------------------------------------------
#include <GUIConfiguration.h>
#include <SetValueDialog.h>
#include <PasswordDialog.h>
#include <UObjectT.h>
#include "Control_SK.h"
#include "UniSetActivator.h"
// -------------------------------------------------------------------------
/*!	Реализация управления */
class Control:
	public Glib::Object,
	public UObjectT<Control_SK>
{
	public:
		Control( uniset::ObjectId id, xmlNode* node, const std::string& prefix );
		virtual ~Control();

		static Glib::RefPtr<Control> create( const std::string& name, const std::string& prefix = "" );

	protected:
		virtual void step();

		int cmdTimeout;
		bool build, ubuild, view,par;

		Glib::RefPtr<MPU::SetValueDialog> dlgSetValue;
		Glib::RefPtr<MPU::UserConf> uconf;
		Glib::RefPtr<MPU::PasswordDialog> dlgPass;

		enum FCControlMode
		{
			fcmSetRPM   = 0,  /*!< режим управления по оборотам */
			fcmSetPower = 1   /*!< режим управления по мощности */
		};

		enum fcState
		{
			fcUndefind = 0, /*!< Неопределенное состояние*/
			fcReady1 = 1, /*!< ГОТОВНОСТЬ1*/
			fcReady2 = 2, /*!< ГОТОВНОСТЬ2*/
			fcStop = 3, /*!< ОСТАНОВЛЕН*/
			fcWork = 4,   /*!< РАБОТА, ХОД, ВРАЩЕНИЕ*/
			fcTurning = 5, /*!< ПРОВОРОТ ВАЛА*/
			fcAlarm = 6  /*!< АВАРИЯ*/
		};

		/* Открыть диалог задачи оборотов  */
		void btnSetRPM_clicked();

		/* Нажатие кнопки Задание по V*/
		void btnVSetting_clicked();
		/* Нажатие кнопки Задание по P*/
		void btnPSetting_clicked();

		/*Задание оборотов и мощности*/
		/* Нажатие кнопки Задание оборотов ГЭД*/
		void btnSetRPMGED_clicked();

		/* Нажатие кнопки Задание ПЧ1*/
		void btnsetPower1_clicked();


		/* Нажатие кнопки Задание ПЧ2*/
		/*void btnsetPower2_clicked();*/

		/*Сбор схемы*/
		/*Собрать схему*/
		void btnBuildScheme_clicked();

		/*Разобрать схему*/
		void btnUBuildScheme_clicked();

		/*ПУСК*/
		void btnPusk_clicked();

		/*СТОП*/
		void btnStop_clicked();


	/* Блокировка/разблокировка управляющих кнопок*/
		void blockcontrol(bool par);

		/* Переключение маленькой мнемосхемы*/
		void mnemoset(bool set);

		/* Установка по мощности(кнопки появляются)*/
		void pset();

		/* Установка по скорости(кнопки появляются)*/
		void vset();


	private:
		Gtk::RadioButton* btnVSetting;
		Gtk::RadioButton* btnPSetting;
		Gtk::Button* btnRPM;
		Gtk::Button* btnBSheme;
		Gtk::Button* btnUBSheme;
		Gtk::Button* btnPusk;
		Gtk::Button* btnStop;
		Gtk::HBox* setFQC;
		Gtk::Button* setPower1;
		Gtk::MessageDialog* qdialog;
		//Gtk::Button* setPower2;
		Gtk::Label* lbl;
		Gtk::Label* lblmain;
		Gtk::Table* tablefqc;
		Gtk::HBox* targetrpm;
		Gtk::Notebook* mnemobook;
		Gtk::Fixed* mainw;

};
// -------------------------------------------------------------------------
#endif // Control_H_
// -------------------------------------------------------------------------
