// -------------------------------------------------------------------------
#ifndef State_H_
#define State_H_
// -------------------------------------------------------------------------
#include <gtkmm.h>
#include <GUIConfiguration.h>
#include <BaseMainWindow.h>
#include <CommonVO.h>
#include "DeviceWindow.h"
#include "DoubleParamWindow.h"
#include "ParamList.h"
using namespace std;
// -------------------------------------------------------------------------
/*!	Реализация управления */
class State:
	public Glib::Object
{
	public:
		State(const string &name, const string &prefix, std::shared_ptr<uniset::SMInterface> &shm,
              MPU::BaseMainWindow *mwin);
		virtual ~State();

		//static Glib::RefPtr<State> create(SMInterface* shm, const std::string& name, const std::string& prefix = "", MPU::BaseMainWindow* mwin = 0);

		void initIterators();
		virtual void poll();

		/* Возвращаем в исходной состояние экран */
		void defaultState();

		void getList(MPU::ParamList::ValList* lst);
	protected:
		virtual void init_first_time();

		Gtk::HBox* mainframe;
		Gtk::VBox* tableState;
		Gtk::VBox* fqclocal;
		Gtk::HBox* fqcmenu;

		MPU::CommonVO* drive1; 	/*!< параметры двигателя */
		MPU::CommonVO* fqc1;		/*!< параметры преобразователя */
		MPU::CommonVO* lsu1;		/*!< параметры ЛСУ */
		MPU::CommonVO* trans1;	/*!< параметры трансформатора */
		MPU::CommonVO* trans3;	/*!< параметры трансформатора */

		MPU::DoubleParamWindow* winDrive1; 	/*!< окно текущих параметров двигателя */
		MPU::DoubleParamWindow* winFQC1; 	/*!< окно текущих параметров преобразователя */
		MPU::DeviceWindow* winLSU1; 		/*!< окно текущих параметров ЛСУ */
		MPU::DeviceWindow* winTrans1; 	/*!< окно текущих параметров Трансформатора TV11*/
		MPU::DeviceWindow* winTrans3; 	/*!< окно текущих параметров Трансформатора TV12*/

		/*Состояние*/
		/*! Нажатие параметры Двигателя 1*/
		void btnDrive1_clicked();

		/* Нажатие параметры ПЧ */
		void btnFQC1_clicked();

		/* Нажатие параметры ЛСУ */
		void btnLSU1_clicked();

		/* Нажатие параметры Трансформатора */
		void btnTrans1_clicked();

		/* Нажатие параметры Трансформатора */
		void btnTrans3_clicked();

		/* Нажатие кнопки Выход */
		void btnStateExit_clicked();

		/* Готовим место */
		void readyState();


	private:
		Gtk::Button* btnDrive1; //кнопка параметры Двигателем
		Gtk::Button* btnFQC1; //кнопка параметры ПЧ
		Gtk::Button* btnLSU1; //кнопка параметры ЛСУ
		Gtk::Button* btnTrans1; //кнопка параметры Трансформатор
		Gtk::Button* btnDrive2; //кнопка параметры Двигателем
		Gtk::Button* btnFQC2; //кнопка параметры ПЧ
		Gtk::Button* btnLSU2; //кнопка параметры ЛСУ
		Gtk::Button* btnTrans2; //кнопка параметры Трансформатор
		Gtk::Button* btnStateExit; //кнопка выхода на экран Состояния
		Gtk::RadioButton* btnFQCLocal; //кнопка выхода на экран ТЕКУЩИЙ
		Gtk::RadioButton* btnFQCRemote; //кнопка выхода на экран УДАЛЕННЫЙ
		Gtk::Button* btnTrans3; //кнопка параметры Трансформатор
		Gtk::Button* btnTrans4; //кнопка параметры Трансформатор
};
// -------------------------------------------------------------------------
#endif // State_H_
