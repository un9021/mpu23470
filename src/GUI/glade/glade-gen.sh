#!/bin/sh
#Генератор glade файлов по шаблону и списку заменяемых датчиков
list=
input=
output=
tempfile=
PROG="${0##*/}"

print_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG [options]

$PROG 	- glade-generate

Valid options are:
  -h, --help	display help screen
  -l, --list	autocorrect list
  -i, --input 	template glade
  -o, --output 	name glade-file
EOF
    [ -n "$1" ] && exit "$1" || exit
}

replace()
{
	while read line; do
		IFS="	"
		set -- $line
		sed -i 's/'$1'/'$2'/g' $output
	done < $list
}

gen()
{
	tempfile='mktemp'
	if [[ -z $output ]]; then
		cp "$input" "$tempfile"
		output=$tempfile
		replace
		cat $output
	else
		cp $input $output
		replace
	fi
	exit 0
}

while getopts "l:i:o:h" opt
do
    case $opt in
	l) list=$OPTARG
	    ;;
	i) input=$OPTARG
	    ;;
	o) output=$OPTARG
	    ;;
	h| --help) print_usage
	    ;;
    esac
done

if [[ -n "$list" && -n "$input" ]]; then
gen
else
	echo "не указаны необходимые параметры"
	print_usage
fi
