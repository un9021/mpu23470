#!/bin/sh

export PATH="${PATH}:./:../../libs/:../../libs/.libs/"

#ulimit -Sc 10000000
START=uniset2-start.sh

GTK2_RC_FILES=./gtkrc LC_ALL=ru_RU.UTF-8 ${START} -g ./mpu23470-gui --smemory-id SharedMemoryGUI \
	--mpu-guifile mpu23470.ui \
	--pass-check-off \
	--no-beep \
	--skip-mb1 \
	--skip-mb2 \
	--dlog-add-levels info,warn,crit $*

#	--mbtcp-filter-field tcp --mbtcp-filetr-value 1 \

#	--dlog-add-levels warn,crit

#	--mbtcp-filter-field mbtcp --mbtcp-filter-value 1

killall get-gate-version.sh
