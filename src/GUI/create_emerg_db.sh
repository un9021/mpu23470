#!/bin/sh

dbname=emergency.db

[ -n "$1" ] && dbname="$1"

sqlite3 $dbname <<"_EOF_"

DROP TABLE IF EXISTS emergency_log;
CREATE TABLE emergency_log (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  aid INTEGER KEY NOT NULL,
  tms timestamp KEY default (strftime('%s', 'now')),
  usec INTEGER default 0,
  msec_step INTEGER NOT NULL
);

DROP TABLE IF EXISTS emergency_records;
CREATE TABLE emergency_records (
  id INTEGER KEY,
  tms timestamp KEY NOT NULL,
  usec INTEGER NOT NULL,
  sid INTEGER NOT NULL,
  val DOUBLE NOT NULL
);

CREATE TRIGGER Delete_Emergency_Trigger
AFTER DELETE ON emergency_log
FOR EACH ROW
BEGIN
    DELETE FROM emergency_records WHERE id = OLD.id;
END;

_EOF_
