// -------------------------------------------------------------------------
#ifndef Setting_H_
#define Setting_H_
// -------------------------------------------------------------------------
#include <GUIConfiguration.h>
#include <PasswordDialog.h>
#include <UniSetActivator.h>
// -------------------------------------------------------------------------
/*!	Реализация управления */
class Setting:
	public Glib::Object
{
	public:
		Setting( uniset::ObjectId id, xmlNode* node, const std::string& prefix );
		virtual ~Setting();

		static Glib::RefPtr<Setting> create( const std::string& name, const std::string& prefix = "" );

	protected:
		Glib::RefPtr<MPU::PasswordDialog> dlgPass;

		/* Открыть меню настройки работы  */
		void btnSetup_clicked();

		/* Закрыть меню настройки работы  */
		void btnSetupExit_clicked();

	private:
		/*Экран Настройка*/
		Gtk::VBox* enMenu;
		Gtk::Fixed* mainMenu;
		Gtk::VBox* journal;
		Gtk::Notebook* notebookjrn;
		Gtk::Button* btnSetup; // Экран Настройки кнопка Параметры рабоыт
		Gtk::Button* btnSetupExit; // Экран Настройки кнопка выход из инженерного меню
};
// -------------------------------------------------------------------------
#endif // Setting_H_
// -------------------------------------------------------------------------
