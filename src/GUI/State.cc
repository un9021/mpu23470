#include "State.h"
// -------------------------------------------------------------------------
using namespace std;
using namespace MPU;
using namespace uniset;
// -------------------------------------------------------------------------
State::State(const string &name, const string &prefix, std::shared_ptr<uniset::SMInterface> &shm, BaseMainWindow *mwin):
	mainframe(nullptr),
	tableState(nullptr),
	drive1(nullptr),
	fqc1(nullptr),
	lsu1(nullptr),
	trans1(nullptr),
	trans3(nullptr),
	winDrive1(nullptr),
	winFQC1(nullptr),
	winLSU1(nullptr),
	winTrans1(nullptr),
	winTrans3(nullptr),
	btnDrive1(nullptr),
	btnFQC1(nullptr),
	btnLSU1(nullptr),
	btnTrans1(nullptr),
	btnDrive2(nullptr),
	btnFQC2(nullptr),
	btnLSU2(nullptr),
	btnTrans2(nullptr),
	btnStateExit(nullptr)
{
	Glib::RefPtr<Gtk::Builder> refXml = GUIConf::GladeXmlInstance();
	drive1 	= new CommonVO("Drive_VO1", shm, refXml);
	fqc1 	= new CommonVO("FQC_VO1", shm, refXml);
	lsu1 	= new CommonVO("LSU_VO1", shm, refXml);
	trans1 	= new CommonVO("Trans_VO1", shm, refXml);
	trans3 	= new CommonVO("Trans_VO3", shm, refXml);

	GETWIDGET(tableState, "tableState");
	GETWIDGET(mainframe, "mainframe");
	GETWIDGET(fqclocal, "mainframe1");

	/* Экран Состояния*/
	cout << "init winDrive1" << endl;
	winDrive1 = new DoubleParamWindow("DriveParamWindow1", tableState, shm, mwin);
	cout << "winDrive1" << endl;
	winDrive1->hide();

	winFQC1 = new DoubleParamWindow("FQCParamWindow1", tableState, shm, mwin);
	winFQC1->hide();

	winLSU1 = new DeviceWindow("LSUViewParamList1", tableState, "LSUParam_VO1", shm);
	winLSU1->hide();

	winTrans1 = new DeviceWindow("TransViewParamList1", tableState, "TransParam_VO1", shm);
	winTrans1->hide();

	winTrans3 = new DeviceWindow("TransViewParamList3", tableState, "TransParam_VO3", shm);
	winTrans3->hide();

	/* Состояние*/
	//Обработчик нажатия кнопка параметры Двигателем 1
	GETWIDGET(btnDrive1, "btnDrive");
	btnDrive1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnDrive1_clicked));

	//Обработчик нажатия кнопка параметры ПЧ 1
	GETWIDGET(btnFQC1, "btnFQC");
	btnFQC1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnFQC1_clicked));

	//Обработчик нажатия кнопка параметры ЛСУ 1
	GETWIDGET(btnLSU1, "btnLSU");
	btnLSU1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnLSU1_clicked));

	//Обработчик нажатия кнопка параметры Трансформатор 1
	GETWIDGET(btnTrans1, "btnTrans1");
	btnTrans1->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans1_clicked));

	//Обработчик нажатия кнопка параметры Трансформатор 3
	GETWIDGET(btnTrans3, "btnTrans3");
	btnTrans3->signal_clicked().connect(sigc::mem_fun(*this, &State::btnTrans3_clicked));

	//Обработчик нажатия кнопки Выход
	GETWIDGET(btnStateExit, "ctl_btn_StateExit");
	btnStateExit->signal_clicked().connect(sigc::mem_fun(*this, &State::btnStateExit_clicked));

}
// -------------------------------------------------------------------------
State::~State()
{
	delete drive1;
	delete fqc1;
	delete lsu1;
	delete trans1;
	delete trans3;
	delete winDrive1;
	delete winLSU1;
	delete winTrans1;
	delete winFQC1;
}
// -------------------------------------------------------------------------
void State::btnDrive1_clicked()
{
	State::readyState();
	winDrive1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnFQC1_clicked()
{
	State::readyState();
	winFQC1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnLSU1_clicked()
{
	State::readyState();
	winLSU1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans1_clicked()
{
	State::readyState();
	winTrans1->win_activate();
}
// -------------------------------------------------------------------------
void State::btnTrans3_clicked()
{
	State::readyState();
	winTrans3->win_activate();
}
// -------------------------------------------------------------------------
void State::btnStateExit_clicked()
{
	mainframe->show();
	//fqcmenu->show();
	tableState->hide();
}
// -------------------------------------------------------------------------
void State::readyState()
{
	mainframe->hide();
	tableState->show();
	winTrans1->win_disactivate();
	winTrans3->win_disactivate();
	winFQC1->win_disactivate();
	winLSU1->win_disactivate();
	winDrive1->win_disactivate();
}
// -------------------------------------------------------------------------
void State::initIterators()
{
	drive1->initIterators();
	fqc1->initIterators();
	lsu1->initIterators();
	trans1->initIterators();
	trans3->initIterators();
	winDrive1->initIterators();
	winFQC1->initIterators();
	winLSU1->initIterators();
	winTrans1->initIterators();
	winTrans3->initIterators();
}
// -------------------------------------------------------------------------
void State::poll()
{
	drive1->poll();
	fqc1->poll();
	lsu1->poll();
	trans1->poll();
	trans3->poll();
	winDrive1->poll();
	winFQC1->poll();
	winLSU1->poll();
	winTrans1->poll();
	winTrans3->poll();
}
// -------------------------------------------------------------------------
void State::getList(ParamList::ValList* lst)
{
	winDrive1->getList(lst);
	winFQC1->getList(lst);
	winLSU1->getList(lst);
	winTrans1->getList(lst);
	winTrans3->getList(lst);
}
// -------------------------------------------------------------------------
void State::init_first_time()
{
	drive1->init_first_time();
	fqc1->init_first_time();
	lsu1->init_first_time();
	trans1->init_first_time();
	trans3->init_first_time();
	winDrive1->init_first();
	winFQC1->init_first();
	winLSU1->init_first();
	winTrans1->init_first();
	winTrans3->init_first();
}
// -------------------------------------------------------------------------
void State::defaultState()
{
	mainframe->show();
	tableState->hide();
	//fqcmenu->show();
}
// -------------------------------------------------------------------------
