configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ctl-gui.sh.in ${CMAKE_CURRENT_SOURCE_DIR}/ctl-gui.sh)

SET(GUI_HEADERS Control_SK.h Setting.h State.h Control.h MainWindow.h)
SET(GUI_SOURCES Control_SK.cc Setting.cc State.cc Control.cc MainWindow.cc mpu-gui.cc)
add_executable(${PACKAGE}-gui ${GUI_HEADERS} ${GUI_SOURCES})

target_link_libraries(${PACKAGE}-gui   ${UNISET_LIBRARIES}
        ${UNISETEXTENSION_LIBRARIES}
        ${UNISETUNET_LIBRARIES}
        ${UNISETMBTCPMASTER_LIBRARIES}
        ${UNISETSHAREDMEMORY_LIBRARIES}
        ${UNISETALGORITHMS_LIBRARIES}
        ${LIBMPU_LIBRARIES}
        ${LIBMPUSM_LIBRARIES}
        ${LIBMPUGUI_LIBRARIES}
        ${GLADEMM_LIBRARIES})

include_directories(./lib)

add_custom_target(gui_configure)
add_custom_command(TARGET gui_configure
        PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${PROJECT_SOURCE_DIR}/src/GUI/guiconfigure.xml ${PROJECT_BINARY_DIR}/src/GUI/guiconfigure.xml
        )

add_custom_target(gui_gtkrc)
add_custom_command(TARGET gui_gtkrc
        PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy ${PROJECT_SOURCE_DIR}/src/GUI/gtkrc ${PROJECT_BINARY_DIR}/src/GUI/gtkrc )


add_custom_target(gui_theme)
add_custom_command(TARGET gui_theme
        PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/src/GUI/theme ${PROJECT_BINARY_DIR}/src/GUI/theme )

add_custom_target(gui_images)
add_custom_command(TARGET gui_images
        PRE_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${PROJECT_SOURCE_DIR}/src/GUI/images ${PROJECT_BINARY_DIR}/src/GUI/images )

add_custom_target(Control_SK.h)
add_custom_command(TARGET Control_SK.h
        PRE_BUILD
        COMMAND uniset2-codegen --ask -n Control --no-main control.src.xml )

add_custom_target(style_gui)
add_custom_command(TARGET style_gui
        PRE_BUILD
        COMMAND astyle -A1 -T -C -S -N -L -w -Y -M -f -p --mode=c --lineend=linux --align-reference=type --align-pointer=type --suffix=none --style=ansi *.cc *.h)


add_dependencies(${PACKAGE}-gui gui_configure)
add_dependencies(${PACKAGE}-gui gui_theme)
add_dependencies(${PACKAGE}-gui gui_images)
add_dependencies(${PACKAGE}-gui gui_gtkrc)
add_dependencies(${PACKAGE}-gui Control_SK.h)
#add_dependencies(${PACKAGE}-gui mpuall_replace)
#add_dependencies(${PACKAGE}-gui style_gui)

install(FILES guiconfigure.xml DESTINATION /etc/${PACKAGE})
install(FILES ${PACKAGE}.ui DESTINATION /usr/share/${PACKAGE})
install(FILES ctl-gui.sh DESTINATION /usr/bin)
install(FILES gtkrc DESTINATION /usr/share/${PACKAGE})
install(FILES alarmdb/alarmdb.xml DESTINATION /usr/share/${PACKAGE}/alarmdb)
install(DIRECTORY theme/ DESTINATION /usr/share/${PACKAGE}/theme)
install(DIRECTORY images/ DESTINATION /usr/share/${PACKAGE}/images)
install(FILES ${PROJECT_SOURCE_DIR}/BUILD/src/GUI/${PACKAGE}-gui DESTINATION /usr/bin)



#add_executable(TestSerialization TestSerialization.cc Setting.cc Setting.h)
#target_link_libraries(TestSerialization ${GTEST_LIBRARIES} Threads::Threads
#        ${UNISET_LIBRARIES}
#        ${UNISETEXTENSION_LIBRARIES}
#        ${UNISETUNET_LIBRARIES}
#        ${UNISETMBTCPMASTER_LIBRARIES}
#        ${UNISETSHAREDMEMORY_LIBRARIES}
#        ${UNISETALGORITHMS_LIBRARIES}
#        ${LIBMPU_LIBRARIES}
#        ${LIBMPUSM_LIBRARIES}
#        ${LIBMPUGUI_LIBRARIES}
#        ${GLADEMM_LIBRARIES})

#enable_testing()
#add_test(TestSerialization "./TestSerialization")
