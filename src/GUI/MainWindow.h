#ifndef _MainWindow_H_
#define _MainWindow_H_
// -------------------------------------------------------------------------
#include <ExtMessageDialog.h>
#include <BaseMainWindow.h>
#include <WarningsWindow.h>
#include "Control.h"
#include "Setting.h"
#include "State.h"
#include "ControlWindow.h"
#include "WinManager.h"
#include "ProfileTime.h"
#include "FlashLed.h"
#include "SaveWindow.h"
// -------------------------------------------------------------------------
using namespace std;
/*!
	Главное окно графического интерфейса.
	В задачи входит:
		- обработка всех приходящих сообщений
		- управление переключением окон
		- и т.п.
    \warning Обработка сообщений ведётся по Gtk-таймеру. Т.к. если создавать собственный поток, то он не синхронизирован с gtk-потоком и это вызывает проблеммы с отрисовкой.
    \TODO
        1. Переделать ClockWindow, чтобы можно было свой glade файл подключать
*/
class MainWindow:
	public MPU::BaseMainWindow
{
	public:
		virtual ~MainWindow();

		static Glib::RefPtr<MainWindow> Instance();
		static Glib::RefPtr<MainWindow> init_gui( uniset::ObjectId id, int argc, char* argv[], std::shared_ptr<uniset::IONotifyController>& nc );

		virtual Gtk::Window* getWin();
		virtual void setFlashButtonsBlock( bool state );
		virtual bool getFlashButtonsBlock();
		virtual std::string saveSensors();

	protected:
		MainWindow( uniset::ObjectId id, std::shared_ptr<uniset::IONotifyController>& nc );
		MPU::UWidgets* bm;

		/* Кнопка Открыть мнемосхему */
		bool btnShow_clicked(GdkEvent* event);

		/* Кнопка Закрыть мнемосхему */
		void btnExit_clicked();


		/* Открыть диалог настройки даты и времени */
		void btnDate_clicked();

		/*Главное меню*/
		/*Кнопка Состояние*/
		void btnState_clicked();
		/*Кнопка Управление*/
		void btnControl_clicked();
		/*Кнопка Настройка*/
		void btnSetting_clicked();

		/*Нижнее меню*/
		/* Нажатие Журнал */
		void btnJournal_clicked();

		/* Журнал */
		/* Кнопка Все в Журнале */
		void btnJournalAll_clicked();

		/* Кнопка Текущие в Журнале */
		void btnJournalNow_clicked();

		/* Кнопка Удаленный в Журнале */
		void btnJournalRem_clicked();

		bool setTime();
		bool setDate();
		void poll();

		virtual void sensorInfo( uniset::SensorMessage* sm );
		virtual void initIterators();
		void on_showAlarmJournal(  const std::string& code, const std::string& datetime, const std::string& numcon, const std::string& bitreg );
		void on_showAlarmWarning(  const std::string& code, const std::string& datetime, const std::string& numcon, const std::string& bitreg );

		Glib::RefPtr<MPU::ChangePasswordDialog> dlgCPass;
		Glib::RefPtr<MPU::WinManager> wman;
		Glib::RefPtr<MPU::WarningsJournal> jwin;
		Glib::RefPtr<MPU::WarningsWindow> wwin;
		Glib::RefPtr<MPU::AlarmWindow> awin;
		Glib::RefPtr<Control> ctl;
		Glib::RefPtr<Setting> set;
		State* sta;

		MPU::ClockWindow* winClock; 	/*!< окно настройки даты и времени */
		MPU::ExtMessageDialog* md;
		MPU::EngineerWindow* ewin; 	/*!< окно инженерного меню */
		MPU::ProcessProgress* sp;
		MPU::FlashLed* flash;

	private:
		static Glib::RefPtr<MainWindow> inst;
		Glib::RefPtr<MPU::SaveWindow> winSave;
		bool on_destroy(GdkEventAny* evnt);

		Gtk::Window* pMainWindow;
		Gtk::Window* pFullScreenSheme;
		Gtk::EventBox* mnemoShemeEv;
		/**********************************/
		Gtk::Button* btnShow; //Открыть мнемосхему
		Gtk::Button* btnExit; //Закрыть мнемосхему
		/**********************************/
		/* Нижнее меню*/
		Gtk::Label* lblTime;
		Gtk::Label* lblDate;

		Gtk::Button* btnMainDate; // Нижнее меню кнопка даты
		Gtk::Button* btnJournal; //кнопка Журнал в нижнем меню
		Gtk::RadioButton* btnJournalall; //кнопка Все в окне Журнала
		Gtk::RadioButton* btnJournalnow; //кнопка Текущие в окне Журнала
		Gtk::RadioButton* btnJournalrem; //кнопка Текущие в окне Журнала

		/* Главное меню*/
		Gtk::RadioButton* btnState;
		Gtk::RadioButton* btnControl;
		Gtk::RadioButton* btnSetting;

		Gtk::Notebook* notebook;
		Gtk::Notebook* notebookjrn;

		sigc::connection connTime;
		sigc::connection connDate;
		Gtk::VBox* journal;

		int psplash;
		int tm_checknetwork_msec;

		void on_new_warning(  const Gtk::TreeModel::iterator&);


		uniset::IOController::IOStateList::iterator ditNetRespond_s, ditLSURespond_s, ditNetRestart_fs, ditPowLim;
};
// -------------------------------------------------------------------------
#endif
// -------------------------------------------------------------------------
