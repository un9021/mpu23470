#!/bin/sh

#ulimit -Sc 10000000
START=uniset2-start.sh

#	 --c-filter-field tsnode --c-filter-value 1 --heartbeat-node ts --lock-value-pause 10 \

${START} -g ./smemory2-mpu23470 \
	--smemory-id SharedMemory1 \
	--dlog-add-levels any \
	--unideb-add-levels any \
	--sm-no-history 1 \
	--skip-can 1 \
	--skip-rs 1 \
	--rs-use-485 0 \
	--rs-dev /dev/null \
	--can-dev /dev/null \
	--mbs1-name MBSlave1 \
	--mbs1-type TCP \
	--mbs1-inet-addr localhost \
	--mbs1-inet-port 2048 \
	--mbs1-filter-field tcpmb \
	--mbs1-filter-value MPU1 \
	--mbs1-reg-from-id 0 \
	--mbs1-force 1 \
	--can--mbreg-propname mbreg \
	--rs-mbreg-propname mbreg \
	--mbs2-name MBSlave2 \
	--mbs2-type TCP \
	--mbs2-inet-addr localhost \
	--mbs2-inet-port 2048 \
	--mbs2-filter-field tcpmb \
	--mbs2-filter-value MPU2 \
	--mbs2-reg-from-id 0 \
	--mbs2-force 1 \
	$*

#	--mbs-filter-field rs --mbs-filter-value 1 \
# --unideb-add-levels info,warn,crit
