// --------------------------------------------------------------------------
#include <string>
#include <sys/wait.h>
#include <Debug.h>
#include <UniSetActivator.h>
#include <ThreadCreator.h>
#include <CanExceptions.h>
#include <modbus/ModbusTypes.h>
#include <MBSlave.h>
#include <MBTCPMaster.h>
#include <UHelpers.h>
#include <Extensions.h>
#include <ExchangeServer.h>
#include "MPUConfiguration.h"
#include <RMonitor.h>
#include <MPUSharedMemory.h>
#include "CommonLamp.h"
// --------------------------------------------------------------------------
using namespace uniset;
using namespace std;
using namespace MPU;

// --------------------------------------------------------------------------
static void help_print(int argc, const char *argv[]);
class ustring
{
public:
    typedef std::list<std::string>			list;
    typedef std::pair<std::string, std::string>	pair;
    typedef std::unordered_map<std::string, std::string>	map;
    static std::string	implode( const std::list<std::string>& arr, const std::string& sep = ",");
    static ustring::list	explode(const std::string& source, const std::string& sep = ",", bool ignore_empty=true);
    static ustring::pair	pair_explode(const std::string& source, const std::string& sep = ",", bool ignore_empty=true);
    static ustring::map	pairs_explode(const std::string& source, const std::string& sep = "=", const std::string& sep2 = ",");
protected:
    ustring(){}
};
//--------------------------------------------------------------------------------------------
std::string ustring::implode( const std::list<std::string>& arr, const std::string& sep )
{
    if(arr.empty())
        return "";

    std::ostringstream temp;
    auto arrIt = arr.begin();
    temp << (*arrIt);
    ++arrIt;

    for(; arrIt != arr.end(); ++arrIt)
    {
        temp << sep << (*arrIt);
    }

    return temp.str();
}
//--------------------------------------------------------------------------------------------
ustring::pair ustring::pair_explode(const std::string& source, const std::string& sep, bool ignore_empty)
{
    string::size_type pos = 0;
    string::size_type prev = 0;
    for(;;)
    {
        pos = source.find_first_of(sep);
        if( pos==string::npos )
            return make_pair(source, "");
        string s(source.substr(prev,pos));
        if( s.empty() && ignore_empty )
            continue;
        string s2(source.substr(pos+1));
        return make_pair(s, s2);
    }
}
//--------------------------------------------------------------------------------------------
ustring::map ustring::pairs_explode(const std::string& source, const std::string& sep, const std::string& sep2)
{
    ustring::map result;
    ustring::list l = ustring::explode(source, sep2);
    ustring::list::reverse_iterator it = l.rbegin();
    for(; it != l.rend(); ++it)
        result.insert(pair_explode(*it, sep));
    return result;
}


// --------------------------------------------------------------------------
ustring::list ustring::explode(const string& source, const string& sep, bool ignore_empty)
{
	ustring::list result;
	
	if( source.empty() )
		return result;
	string::size_type prev = 0;
	string::size_type pos = 0;
	while( pos!=string::npos )
	{
		pos = source.find_first_of(sep,prev);
		string::size_type num = string::npos;
		if( pos!=string::npos )
			num = pos-prev;
		string s(source.substr(prev,num));
		if( !s.empty() || !ignore_empty )
			result.emplace_back(s);
		prev=pos+1;
	}
	
	return result;
}
// --------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    try {
        auto conf = uniset::uniset_init(argc, argv, "configure.xml");

        auto act = UniSetActivator::Instance();

        conf->initLogStream(dlog, "dlog");
        // ------------ SharedMemory ----------------
        auto shm = MPUSharedMemory::init_mpusmemory(argc, argv);

        if (!shm)
            return 1;

        act->add(shm);

        // ------------- FQC Exchange --------------
        auto es = ExchangeServer::init_exchange(argc, const_cast<char **>(argv), shm);

        if (!es)
            return 1;

        shm->logAgregator()->add(es->logAgregator());

        auto es_thr = new ThreadCreator<ExchangeServer>(es.get(), &ExchangeServer::execute);

        if (!es_thr)
            return 1;

        auto rmonit = uniset::make_object<RMonitor>("RMonitor", "RMonitor", es);
        act->add(rmonit);

        // ------------- ModbusTCP Slave (GUI) --------------
        bool skip_mbs = findArgParam("--skip-mbs", argc, argv) != -1;

        if (!skip_mbs) {
            auto mbs = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs");

            if (!mbs)
                return 1;

            shm->logAgregator()->add(mbs->log());
            act->add(mbs);
        }

        // ------------- ModbusTCP Slave2 (GUI) --------------
        bool skip_mbs2 = findArgParam("--skip-mbs2", argc, argv) != -1;

        if (!skip_mbs2) {
            auto mbs2 = MBSlave::init_mbslave(argc, argv, shm->getId(), shm, "mbs2");

            if (!mbs2)
                return 1;

            shm->logAgregator()->add(mbs2->log());
            act->add(mbs2);
        }

        auto cl_list = ustring::explode(getArgParam("--commonlamp-list", argc, argv, "" ),",");
		for( auto it=cl_list.begin(); it!=cl_list.end(); it++ )
		{
			auto cl = make_object<CommonLamp>(getArgParam((*it) + "-commonlamp-name", argc, argv, "GateCommonLamp" ), getArgParam((*it) + "-commonlamp-node-name", argc, argv, "CommonLamp" ));
			
			if( !cl )
			{
				cout << "Ошибка при создании CommonLamp " << (*it) << endl;
				return 1;
			}
			
			act->add(cl);
		}
		
        // ----------------------------------------
        // попытка решить вопрос с "зомби" процессами
        signal(SIGCHLD, MPU::on_sigchild);

        if (es_thr)
            es_thr->start();

        SystemMessage sm(SystemMessage::StartUp);
        act->broadcast(sm.transport_msg());
        act->run(false);

        MPU::on_sigchild(SIGTERM);
        return 0;
    }
    catch (uniset::ModbusRTU::mbException &ex) {
        cerr << "(smemory2): " << ex << endl;
    }
    catch (uniset::Exception &ex) {
        cerr << "(smemory2): " << ex << endl;
    }
    catch (CORBA::SystemException &ex) {
        cerr << "(smemory2): " << ex.NP_minorString() << endl;
    }
    catch (std::exception &e) {
        cerr << "(smemory2): " << e.what() << endl;
    }
    catch (...) {
        cerr << "(smemory2): catch(...)" << endl;
    }

    MPU::on_sigchild(SIGTERM);
    return 1;
}

// --------------------------------------------------------------------------
void help_print(int argc, const char *argv[]) {
    //	cout << "--logfile fname	- выводить логи в файл fname. По умолчанию smemory.log" << endl;
    //	cout << "--datfile fname	- Файл с картой датчиков. По умолчанию configure.xml" << endl;
    //	cout << "--skip-can			- пропустить запуск CAN" << endl;
    //	cout << "--skip-rs		- пропустить запуск RS" << endl;
    cout << "--skip-mb     - skip MBTCPSlave for mpu gui" << endl;

    cout << "\n   ###### SM options ###### \n" << endl;
    MPUSharedMemory::help_print(argc, argv);

    cout << "\n   ###### Exchange options ###### \n" << endl;
    ExchangeServer::help_print(argc, argv);
}
