#!/bin/sh
# Обновление образов (флэшек) на узлах из заданного каталога с образами

RETVAL=0
PROG="${0##*/}"
ACTION="write-image"

. ctl-conf.sh

print_write_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG IMGDIR [ $NODES ] | all

EOF
    [ -n "$1" ] && exit "$1" || exit
}

[ -z "$1" ] && print_write_usage 1
IMGDIR=$1; shift;

if [ -d $IMGDIR ]; then
    echo "find image in '$IMGDIR'.."
else
    echo "$IMGDIR not found!" && exit 1
fi

set_list "$*"

for HOST in $LIST; do

    msg_log " starting..."
    init_vars "$ACTION" "$HOST"
    pre_log >/dev/null
    
	IMAGE=`find $IMGDIR -name *$HOST*.gz -type f 2>/dev/null | head -n 1`

	if [ -z "$IMAGE" ]; then
		msg_log "Image for '$HOST' not found in '$IMGDIR'"
		fail_log
	else
		if ping -c 5 -i 0.4 -w 3 $HOST 1>>"$LOG" 2>>"$LOG"; then
	
			DISK=`ssh $SSHOPT $KSSH -x $SSHUSER@$HOST 'cat /etc/lilo.conf | grep boot= | sed -e "s|boot=\(.*\)$|\\1|i"'`
	    	if [ -z "$DISK" ]; then
				msg_log "ERROR: UNKNOWN DISK"
				fail_log
	    	else
	            #ssh $SSHOPT $KSSH -x $SSHUSER@$HOST mount -o remount,ro /
				if cat $IMAGE | gzip -d | ssh $SSHOPT $KSSH -x $SSHUSER@$HOST "cat - > $DISK"; then
					ok_log
					msg_log "reboot.."
		            ssh $SSHOPT $KSSH -x $SSHUSER@$HOST reboot
				else
					fail_log
				fi &
			fi 
		else
			msg_log "ERROR: NODE NOT RESPOND."
			fail_log
		fi
    fi
done

#wating
wait

exit $RETVAL
