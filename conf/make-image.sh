#!/bin/sh
# Создание образов флэшек со всех узлов

RETVAL=0
PROG="${0##*/}"
ACTION="make-image"

. ctl-conf.sh

set_list "$*"

IMGNAME=$PROJECT
IMGDIR=$HOME/images
DDOPT512MB="bs=1MB count=504"
DDOPT1GB="bs=1M count=1000"

PID=

HEAD=`date "+%Y%m%d_%H%M%S"`

IMG_HEAD=`date "+%Y-%m-%d"`
IMGSUBDIR=`date "+%Y-%m-%d-%H-%M"`

mkdir -p $LOGDIR
mkdir -p $IMGDIR/$IMGSUBDIR

# install
for HOST in $LIST; do
	msg_log " starting..."
	init_vars "$ACTION" "$HOST"
    pre_log >/dev/null

	IMAGE=$IMGDIR/$IMGSUBDIR/$IMGNAME.$IMG_HEAD.$HOST.img.gz

	if ping -c 5 -i 0.4 -w 3 $HOST 1>>"$LOG" 2>>"$LOG"; then
	
		# blkid | sed -e 's|/dev/\(.*\):\(.*\)$|/dev/\1|i'
		IMGDEV=`ssh $KSSH -x $SSHUSER@$HOST 'cat /etc/lilo.conf | grep boot= | sed -e "s|boot=\(.*\)$|\\1|i"'`
		
		if [ -n "$IMGDEV" ]; then
			test_gui=`echo $HOST | grep -o gui`
			if [ "$test_gui" == "gui" ]; then
				DDOPT=$DDOPT1GB
			else
				DDOPT=$DDOPT512MB
			fi
			if ssh $KSSH -x $SSHUSER@$HOST "dd status=none if=$IMGDEV $DDOPT" | gzip | cat - > $IMAGE; then
				ok_log
			else
				fail_log
			fi &
		else
			msg_log "ERROR: UNKOWN DISK"
			fail_log
		fi
	else
		msg_log "ERROR: NODE NOT RESPOND"
		fail_log
	fi

	PID=$!
done

#wating
wait

exit $RETVAL
