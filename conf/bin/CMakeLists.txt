configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ctl-functions-${PACKAGE}.sh.in ${CMAKE_CURRENT_SOURCE_DIR}/ctl-functions-${PACKAGE}.sh)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ctl-install.sh.in ${CMAKE_CURRENT_SOURCE_DIR}/ctl-install.sh)
#configure_file(${CMAKE_CURRENT_SOURCE_DIR}/ctl-update.sh.in ${CMAKE_CURRENT_SOURCE_DIR}/ctl-update.sh)

install(FILES ctl-functions-${PACKAGE}.sh DESTINATION /usr/bin/)
install(FILES ctl-install.sh DESTINATION /usr/bin/)
#install(FILES ctl-update.sh DESTINATION /usr/bin/)
