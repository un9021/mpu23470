%def_disable doc

Name: mpu23470
Version: 0.1
Release: set0.137
Summary: MPU 23470

License: GPL
Group: Development/C++
Url: http://setri.ru

Source: %name-%version.tar

Requires: mpulib3 >= 3.1-set31

# Automatically added by buildreq on Wed Aug 25 2010
BuildRequires: libXtst-devel libglademm-devel python-modules mpulib3-devel

%description
MPU. This is special internal project for SET Research Instinute

%package gate
Summary: Interface converter for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: libuniset2-extension-sqlite
Requires: mpu23470
Requires: mpulib3-gate
Requires: mpu3-gate

%description gate
Converter station for MPU project

%package gui
Summary: Common package for MPU
Group: Development/Other
Requires: %name = %version-%release
Requires: autologin
Requires: xinit
Requires: mpu23470
Requires: mpulib3-gui
Requires: mpu3-gui

%description gui
GUI for for MPU

%package conf1
Summary: Configure package for MPU1
Group: Development/Other
Requires: %name = %version-%release

%description conf1
Configure for for MPU1

%package conf2
Summary: Configure package for MPU2
Group: Development/Other
Requires: %name = %version-%release

%description conf2
Configure for for MPU2

%package doc
Summary: Documentation for MPU
Group: Development/Other
Requires: %name = %version-%release

%description doc
Documentation for MPU Project

%prep
%setup -q
%build
%cmake
cd ./BUILD
%make

#%autoreconf
#configure

%install
cd ./BUILD
%makeinstall_std
mkdir -p -m755 %buildroot/var/local/log/%name
mkdir -p -m755 %buildroot/var/local/run/%name
mkdir -p -m755 %buildroot/var/local/lock/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/cache/%name/{system,alarmdb}
mkdir -p -m755 %buildroot/var/local/omniORB
mkdir -p -m644 %buildroot%_datadir/%name
mkdir -p -m755 %buildroot/var/local/%name/db
mkdir -p -m755 %buildroot/var/local/%name/backup

mkdir -p -m755 %buildroot%_initdir

mkdir -p -m755 %buildroot%_datadir/mpu3
echo "%name-%version-%release" > %buildroot%_datadir/mpu3/VERSION.txt
echo "%name-%version-%release" > %buildroot%_datadir/%name/VERSION.txt

subst "s|img=\"|img=\"/usr/share/mpu23470/images/|g" %buildroot/etc/%name/guiconfigure.xml
subst "s|images/images|images|g" %buildroot/etc/%name/guiconfigure.xml

%post
ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu.sh
cat %_sysconfdir/openssh/authorized_keys2/23470.pub > %_sysconfdir/openssh/authorized_keys2/root

# необходимо для скриптов ctl-XXXtohrml, и работы системы "сброса" журналов
ln -sf %_sysconfdir/%name/configure.xml %_sysconfdir/mpu3/configure.xml



ln -sf %_bindir/ctl-admin-%name.sh %_bindir/ctl-admin-mpu3.sh
ln -sf %_datadir/%name/alarmdb %_datadir/mpu3/alarmdb

%post gui
sed -e "s|<LocalNode name=\".*\"|<LocalNode name=\"GUINode\"|i" %_sysconfdir/%name/configure.xml > %_sysconfdir/%name/configure.xml.o
mv -f %_sysconfdir/%name/configure.xml.o %_sysconfdir/%name/configure.xml
#subst "s|SharedMemory1|SharedMemoryGUI|g" %_bindir/ctl-smviewer-%name.sh
sed -e "s:dlgSetVal=\"mpu23470:dlgSetVal=\"/usr/share/mpu23470/mpu23470:g" %_sysconfdir/%name/guiconfigure.xml > %_sysconfdir/%name/guiconfigure.xml.o
mv -f %_sysconfdir/%name/guiconfigure.xml.o %_sysconfdir/%name/guiconfigure.xml
chmod +x /usr/bin/ctl-*
subst "s|SharedMemory1||g" %_bindir/ctl-smviewer-%name.sh
subst "s|HOSTNAME=.*|HOSTNAME=gui|g" %_sysconfdir/sysconfig/network
subst "s|<LocalNode name=\"LocalhostNode\"|<LocalNode name=\"GUINode\"|g" %_sysconfdir/mpu23470/configure.xml
chmod +x /usr/bin/%name-gui
mv -bf %_sysconfdir/%name/fstab.gui %_sysconfdir/fstab

subst "s|<LocalIOR name=\"0\"/>|<LocalIOR name=\"1\"/>|g" /etc/%name/configure.xml
subst "s|<ConfDir name=\"./\"/>|<ConfDir name=\"/etc/%name/\"/>|g" /etc/%name/configure.xml
subst "s|<DataDir name=\"./\"/>|<DataDir name=\"/usr/share/%name/\"/>|g" /etc/%name/configure.xml
subst "s|<BinDir name=\"./\"/>|<BinDir name=\"/usr/bin/\"/>|g" /etc/%name/configure.xml
subst "s|<LogDir name=\"./\"/>|<LogDir name=\"/var/local/log/\"/>|g" /etc/mpu23470/configure.xml
subst "s|<LockDir name=\"./\"/>|<LockDir name=\"/var/local/lock/\"/>|g" /etc/mpu23470/configure.xml
echo %name-%version-%release > %_sysconfdir/motd

%post gate
subst "s|HOSTNAME=.*|HOSTNAME=gate|g" %_sysconfdir/sysconfig/network
subst "s|<LocalNode name=\"LocalhostNode\"|<LocalNode name=\"GATENode\"|g" %_sysconfdir/mpu23470/configure.xml
chmod +x /usr/bin/ctl-*
chmod +x /usr/bin/smemory2-mpu23470

ln -sf %_sysconfdir/%name/gate.modules %_sysconfdir/modules
ln -sf %_sysconfdir/%name/gate.can200.conf %_sysconfdir/modprobe.d/can200.conf
ln -sf /usr/bin/ctl-smemory2-mpu23470.sh /usr/bin/ctl-smemory2-mpu3.sh

subst "s|<LocalIOR name=\"0\"/>|<LocalIOR name=\"1\"/>|g" /etc/%name/configure.xml
subst "s|<ConfDir name=\"./\"/>|<ConfDir name=\"/etc/%name/\"/>|g" /etc/%name/configure.xml
subst "s|<DataDir name=\"./\"/>|<DataDir name=\"/usr/share/%name/\"/>|g" /etc/%name/configure.xml
subst "s|<BinDir name=\"./\"/>|<BinDir name=\"/usr/bin/\"/>|g" /etc/%name/configure.xml
subst "s|<LogDir name=\"./\"/>|<LogDir name=\"/var/local/log/\"/>|g" /etc/mpu23470/configure.xml
subst "s|<LockDir name=\"./\"/>|<LockDir name=\"/var/local/lock/\"/>|g" /etc/mpu23470/configure.xml
echo %name-%version-%release > %_sysconfdir/motd
mv -bf %_sysconfdir/%name/fstab.gate %_sysconfdir/fstab

%post conf1
mv -bf %_sysconfdir/%name/hosts.conf1 %_sysconfdir/hosts

%post conf2
mv -bf %_sysconfdir/%name/hosts.conf2 %_sysconfdir/hosts

%postun

%files
%config %_sysconfdir/%name/guiconfigure.xml
%_sysconfdir/openssh/authorized_keys2/23470.pub
%_sysconfdir/%name/gate.modules
%_sysconfdir/%name/gate.can200.conf
%_sysconfdir/%name/hosts*
#%_sysconfdir/%name/fstab
%config %_sysconfdir/mpu3/userconf.d/mpu23470.conf

%dir %_datadir/%name/
%_datadir/%name/VERSION.txt
%_datadir/mpu3/VERSION.txt
%_datadir/%name/alarmdb/alarmdb.xml
%_datadir/%name/theme/*
%_datadir/%name/images/*
%_datadir/%name/gtkrc
%exclude %_bindir/smemo*
%_bindir/ctl-admin-mpu23470.sh
%_bindir/ctl-functions-mpu23470.sh
%_bindir/ctl-install.sh
%_bindir/ctl-smonit-mpu23470.sh
%_bindir/ctl-smviewer-mpu23470.sh
#%_bindir/ctl-update.sh


%attr(0777,root,root) %dir /var/local/log/%name
%attr(0777,root,root) %dir /var/local/run/%name
%attr(0777,root,root) %dir /var/local/lock/%name
%attr(0777,root,root) %dir /var/local/cache/%name
%attr(0777,root,root) %dir /var/local/omniORB/
%attr(0777,guest,root) %dir /var/local/%name/db
%attr(0777,guest,root) %dir /var/local/%name/backup

%files -n %name-gate
%_bindir/smemory2-*
%config %_sysconfdir/%name/configure.xml
%_bindir/ctl-smemory2-mpu23470.sh
%_sysconfdir/%name/fstab.gate


%files -n %name-gui
%_bindir/mpu23470-gui
%_datadir/%name/*.ui
%_bindir/ctl-gui.sh
%_sysconfdir/%name/fstab.gui
%config %_sysconfdir/%name/configure.xml

%files conf1

%files conf2

%if_enabled doc
%files -n %name-doc
%_docdir/%name-%version/
%endif

%changelog
* Sat Jun 22 2019 Ura Nechaev <nechaev@server> 0.1-set0.101
- new build

* Fri Jun 14 2019 Ura Nechaev <nechaev@server> 0.1-set0.84
- new build

* Tue Jun 04 2019 un 0.1-set0.58
- new build

* Tue Jun 04 2019 un 0.1-set0.58
- new build

* Tue Jun 04 2019 un 0.1-set0.58
- new build

* Tue Jun 04 2019 un 0.1-set0.58
- new build

* Tue Jun 04 2019 un 0.1-set0.58
- new build

