#!/bin/sh


PROJECT=22220
NODES="mpu23470.gui1 mpu23470.gate1 mpu23470.gui2 mpu23470.gate2"
IM_NODES="gui1 gui2 gate1 gate2"
# umptester"

LOGDIR=$HOME/logs/$PROJECT
DEBUG=
SSHUSER=root
SSHOPT="-O StrictHostKeychecking=no -O ForwardX11=no"
ACTION=${ACTION-action}

KSSH=
[ -z "$DEBUG" ] && KSSH="-T" || KSSH=

# pssh 
PSSH_MAX_THREAD=4
PSSH_TIMEOUT=60  # sec

print_usage()
{
	[ "$1" = 0 ] || exec >&2
	cat <<EOF
Usage: $PROG [ $NODES ] | all

EOF
    [ -n "$1" ] && exit "$1" || exit
}

ACTION=${ACTION-}
HOST=${HOST-}
LOG="$LOGDIR/$HOST.$ACTION.log"
init_vars()
{
	ACTION="$1"
	HOST="$2"
	mkdir -p $LOGDIR
	LOG="$LOGDIR/$HOST.$ACTION.log"
}

LIST=
set_list()
{
	if [ "$1" == "all" ]; then
		LIST=$NODES
	else
		LIST="$*"
	fi
	[ -z "$LIST" ] && print_usage 1
}

pre_log()
{
	local WHAT=${1-starting $ACTION}
	local HEAD=`date "+%Y-%m-%d %H:%M"`
	echo "'$HOST' $WHAT..."
	echo " $HEAD '$LOG'..."

	echo "                     ">>$LOG
	echo "=====================">>$LOG
	echo $HEAD>>$LOG
	echo "=====================">>$LOG
}

ok_log()
{
	echo "'$HOST' $ACTION OK">>$LOG 
	echo "'$HOST' $ACTION OK"
}

fail_log()
{
	echo "'$HOST' $ACTION FAILED. See log '$LOG' for details..."
	echo "'$HOST' $ACTION FAILED. ">>$LOG
}


msg_log()
{
	echo "'$HOST' $ACTION: $1">>$LOG 
	echo "'$HOST' $ACTION: $1"
}

init_pssh_vars()
{
	
	#  hlist=`mktemp`
	#  из-за запуска через pssh exec процесс уже не возвращается "сюда" и временный файл не удалить
	#  поэтому используем не mktemp, а 'жёсткое' имя файла... (чтобы если и "засорять" TMP то только одним файлом)
	hlist=$TMPDIR/$PROJECT.update.hostslist
	echo "${LIST}" | sed 's| |\n|g' > ${hlist}
}

comment_dialog()
{
	TOFILE=comment.read.me
	[ -n "$1" ] && TOFILE="$1"

	echo "***                                                    ***"
	echo "***           Напишите пожалуйста комментарий          ***"
	echo "----------------------------------------------------------"
	echo "/ Ctrl+C или три раза ENTER                              /"
	echo "----------------------------------------------------------"
	echo

	bnum=1

	while read item; do
		
		[ -z "$item" ] && let bnum=$bnum+1 || bnum=1
		
		[ "$bnum" -gt 2 ] && break;
		
		echo "$item" >> $TOFILE
	done
}