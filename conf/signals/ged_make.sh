#!/bin/bash
. make_common.sh
[ -z "$correctlist" ] && correctlist=common_rules
input=$1
number=$2
shift_number=300
[ -z "$number" ] && number=11000
cur_number=${number};
PROG="${0##*/}"

print_help()
{
      [ "$1" = 0 ] || exec >&2
      cat <<EOF
      Usage: $PROG name_file [BEGIN_ID]
      name_file - input file(*.csv with separator ";")
      BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1
numb_gen=1
rules=$(get_autozamena_sed_rules "$correctlist")
extra_rules=
for GED in "GED1" "GED2" "GED3"; do
	gedl=$(echo $GED | tr '[:upper:]' '[:lower:]')
	echo "			<!-- $GED -->"
	
	if [[ $GED == "GED1" ]]; 
	then 
            drive_rules="s:winDrive:winDrive1:g;s:winDrive1Temp:winDriveTemp1:g;"
			extra_rules="s:gedX:ged1:g;"
            filter_name1="ged11"
            filter_name2="ged12"
            filter_temp1="ged11_temp"
            filter_temp2="ged12_temp"

            RESPOND1="GED11_"
            RESPOND2="GED12_"
            filter_name_temp="ged1"


            GEU="GEU1"

            ged11_ru="ГЭД11"
            ged12_ru="ГЭД12"
            ged11="GED11"
            ged12="GED12"

            arm_mbreg3="arm_mbreg_fault"
            arm_mbreg2="arm_mbreg_fault"
            arm_mbreg1="arm_mbreg"
            filter_name_alarm1="geu11_alarm"
            filter_name_alarm2="geu12_alarm"
            filter_name_alarm3="geu1_alarm"

            filter_name_fault1="geu11_fault"
            filter_name_fault2="geu12_fault"
            filter_name_fault3="geu1_fault"


            filter_name_ged_fault1="ged11_fault"
            filter_name_ged_fault2="ged12_fault"
            
            
            filter_name_ged_fault2="ged22_fault"
            
            filter_name_bearing_alarm1="ged1_bearing_drive"
	    filter_name_bearing_alarm2="ged1_bearing_ndrive"
	    
	    filter_name_bearing_fault1="ged1_bearing_drive"
	    filter_name_bearing_fault2="ged1_bearing_ndrive"
	    
	    filter_name_bearing_norm1="ged1_bearing_drive"
	    filter_name_bearing_norm2="ged1_bearing_ndrive"
	    
	fi
	if [[ $GED == "GED2" ]]; 
	then 
            drive_rules="s:winDrive:winDrive2:g;s:winDrive2Temp:winDriveTemp2:g;"
			extra_rules="s:gedX:ged2:g;"
            filter_name1="ged21"
            filter_name2="ged22"
            filter_temp1="ged21_temp"
            filter_temp2="ged22_temp"
            RESPOND1="GED21_"
            RESPOND2="GED22_"
            filter_name_temp="ged2"

            GEU="GEU2"


            ged11_ru="ГЭД21"
            ged12_ru="ГЭД22"
            ged11="GED21"
            ged12="GED22"

            arm_mbreg3="arm_mbreg_fault"
            arm_mbreg1="arm_mbreg_fault"
            arm_mbreg2="arm_mbreg"

            filter_name_alarm1="geu21_alarm"
            filter_name_alarm2="geu22_alarm"
            filter_name_alarm3="geu2_alarm"

            filter_name_fault1="geu21_fault"
            filter_name_fault2="geu22_fault"
            filter_name_fault3="geu2_fault"


            filter_name_ged_fault1="ged21_fault"
            filter_name_ged_fault2="ged22_fault"
	    
	    filter_name_bearing_alarm1="ged2_bearing_drive"
	    filter_name_bearing_alarm2="ged2_bearing_ndrive"
	    
	    filter_name_bearing_fault1="ged2_bearing_drive"
	    filter_name_bearing_fault2="ged2_bearing_ndrive"
	    
	    filter_name_bearing_norm1="ged2_bearing_drive"
	    filter_name_bearing_norm2="ged2_bearing_ndrive"

	fi
	if [[ $GED == "GED3" ]]; 
	then 
            drive_rules="s:winDrive:winDrive3:g;s:winDrive3Temp:winDriveTemp3:g;"
			extra_rules="s:gedX:ged3:g;"

            filter_name1="ged31"
            filter_name2="ged32"
            filter_temp1="ged31_temp"
            filter_temp2="ged32_temp"

            RESPOND1="GED31_"
            RESPOND2="GED32_"

            filter_name_temp="ged3"


            GEU="GEU3"



            ged11_ru="ГЭД31"
            ged12_ru="ГЭД32"
            ged11="GED31"
            ged12="GED32"

            arm_mbreg1="arm_mbreg_fault"
            arm_mbreg2="arm_mbreg_fault"
            arm_mbreg3="arm_mbreg"

            filter_name_alarm1="geu31_alarm"
            filter_name_alarm2="geu32_alarm"
            filter_name_alarm3="geu3_alarm"

            filter_name_fault1="geu31_fault"
            filter_name_fault2="geu32_fault"
            filter_name_fault3="geu3_fault"

            filter_name_ged_fault1="ged31_fault"
            filter_name_ged_fault2="ged32_fault"
            
            
            filter_name_bearing_alarm1="ged3_bearing_drive"
	    filter_name_bearing_alarm2="ged3_bearing_ndrive"
	    
	    filter_name_bearing_fault1="ged3_bearing_drive"
	    filter_name_bearing_fault2="ged3_bearing_ndrive"
	    
	    filter_name_bearing_norm1="ged3_bearing_drive"
	    filter_name_bearing_norm2="ged3_bearing_ndrive"
            
            
	fi
	
	while read line; do
		echo $line | sed "s:names_geu:$GEU:g;s:ged11_ru:$ged11_ru:g;s:ged12_ru:$ged12_ru:g;s:ged11:$ged11:g;s:ged12:$ged12:g;s:GEU\([123]\)_ru:ГЭУ\1:g; s:names:$GED:g;s:GED\([123]\)_ru:ГЭД\1:g;  s:respond1_:$RESPOND1:g;s:respond2_:$RESPOND2:g;s:arm_mbreg1:$arm_mbreg1:g;s:arm_mbreg2:$arm_mbreg2:g;s:arm_mbreg3:$arm_mbreg3:g;   s:filter_name1:$filter_name1:g; s:filter_name2:$filter_name2:g; s:filter_name_temp:$filter_name_temp:g; s:filter_temp1:$filter_temp1:g; s:filter_temp2:$filter_temp2:g;  s:filter_name_alarm1:$filter_name_alarm1:g;  s:filter_name_alarm2:$filter_name_alarm2:g;s:filter_name_alarm3:$filter_name_alarm3:g;
		s:filter_name_fault1:$filter_name_fault1:g;s:filter_name_fault2:$filter_name_fault2:g;s:filter_name_fault3:$filter_name_fault3:g; 
		s:filter_name_ged_fault1:$filter_name_ged_fault1:g; s:filter_name_ged_fault2:$filter_name_ged_fault2:g;		
		s:filter_name_bearing_alarm1:$filter_name_bearing_alarm1:g; s:filter_name_bearing_alarm2:$filter_name_bearing_alarm2:g;
		s:filter_name_bearing_fault1:$filter_name_bearing_fault1:g; s:filter_name_bearing_fault2:$filter_name_bearing_fault2:g;
		s:filter_name_bearing_norm1:$filter_name_bearing_norm1:g; s:filter_name_bearing_norm2:$filter_name_bearing_norm2:g;
		s:tcp_unet:geu$numb_gen:g;s:$:\"/>:g;s:id;:			<item id=\"$cur_number:g;${rules};$drive_rules;${extra_rules}"
		cur_number=$((cur_number+1))
	done < $input
	
	cur_number=$((number+shift_number))
	number=${cur_number};
	numb_gen=$((numb_gen+1))
done
