#!/bin/bash

. make_common.sh
[ -z "$correctlist" ] && correctlist=common_rules
input=$1
number=$2
[ -z "$number" ] && number=15000
inc_number=100
#Правила замены для разных TVX
TVX_RULES=
#Для красивой вставки номера TV
ARRAY_TV=( 11 12 21 22 )
num_tv=
shift_number=${number};
#Для правильной нумерации
skip_shift=
PROG="${0##*/}"

print_help()
{
      [ "$1" = 0 ] || exec >&2
      cat <<EOF
	  Usage: $PROG name_file [BEGIN_ID]
      name_file - input file(*.csv with separator ";")
      BEGIN_ID - first id item. Default: $number

	  output_file name "tvx_sensors.xml"
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[ -z "$1" ] && print_help 1

#rm -f tv_sensors.xml

rules=$(get_autozamena_sed_rules "$correctlist")
for TVX in "TV1" "TV2" "TV3" ; do

	case "$TVX" in
	"TV1" )
				TVX_RULES=";s|tcp_unet|geu1|g;s|gedx_io|ged11|g;s|gedx_prot|ged12|g;s|filter_temp1|ged11_temp|g;s|filter_temp2|ged12_temp|g;s|winTrans1|winTrans1TV1|g;s|winTrans2|winTrans2TV1|g;s|winTrans3|winTrans3TV1|g;s|winTrans4|winTrans4TV1|g"
		num_tv=0 ;;
	"TV2" )
				TVX_RULES=";s|tcp_unet|geu2|g;s|gedx_io|ged21|g;s|gedx_prot|ged22|g;s|filter_temp1|ged21_temp|g;s|filter_temp2|ged22_temp|g;s|geu11_alarm|geu21_alarm|g;s|geu12_alarm|geu22_alarm|g;s|geu11_fault|geu21_fault|g;s|geu12_fault|geu22_fault|g ;s|tv1|tv2|g;s|winTrans1|winTrans1TV2|g;s|winTrans2|winTrans2TV2|g;s|winTrans3|winTrans3TV2|g;s|winTrans4|winTrans4TV2|g"
		num_tv=0 ;;
	"TV3" )
				TVX_RULES=";s|tcp_unet|geu3|g;s|gedx_io|ged31|g;s|gedx_prot|ged32|g;s|filter_temp1|ged31_temp|g;s|filter_temp2|ged32_temp|g;s|geu11_alarm|geu31_alarm|g;s|geu12_alarm|geu32_alarm|g;s|geu11_fault|geu31_fault|g;s|geu12_fault|geu32_fault|g;s|tv1|tv3|g;s|winTrans1|winTrans1TV3|g;s|winTrans2|winTrans2TV3|g;s|winTrans3|winTrans3TV3|g;s|winTrans4|winTrans4TV3|g"
		num_tv=0 ;;
	esac

	while read line; do
		if echo $line | grep -q "^;\{1,\}$"; then
		echo "			<!-- ********************** Блок датчиков $TVX${ARRAY_TV[$num_tv]} ********************** -->";
		num_tv=$((num_tv+1))
		[[ -n "$skip_shift" ]] && let number=$((shift_number+inc_number)) && shift_number=$number;
		skip_shift=1
		else
		echo $line | sed "s|TVX|$TVX|g;s|id;|			<item id=\"$number|g${rules}${TVX_RULES}"
		number=$((number+1))
		fi
	done < $input
done
