#!/bin/bash

#Скрипт-болванка для создания ..._make.sh

. make_common.sh

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=3
#Сдвиговый параметр, на сколько будет отступ между датчиками (например, между TV1 и TV2)

shift_number=$number

PROG="${0##*/}"
#inc_number=5000

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")
write="w\;1"
conf="c\;1"
inc_number=5000
inc_numconf=300
OBJECT="MPU1"

gen_w()
{
        if  echo $line | grep -q $write 
        then
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU|$OBJ|g;s|from|to|g;s|0x04|0x10|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        fi
}

gen()
{
        namewarn=`echo $line | sed "s|;textname.*||g;s|.*name;||g;s|_S|_Warn_S|g;s|_AS|_Warn_AS|g"`
        namealarm=`echo $line | sed "s|;textname.*||g;s|.*name;||g;s|_S|_Alarm_S|g;s|_AS|_Alarm_AS|g"`
        if  echo $line | grep -q $conf 
        then
        echo $line | sed "s|id;|		<item id=\"$number|g;s|\;textname\;|\" sidWarn=\"conf_$namewarn\" sidAlarm=\"conf_$namealarm\;textname\;|g;s|<!--|		<!--|g;s|MPU|$OBJ|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        else
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU|$OBJ|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        fi
}

gen_c()
{
        
        if  echo $line | grep -q $conf 
        then
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU_|conf_$OBJECT\_|g;s|\;MPU\;|\;$OBJECT\;|g;s|_S|_Alarm_S|g;s|_AS|_Alarm_AS|g;s|fqcTemp\;1\;winFQCTemp\;1\;||g;s|\;tcp_mbtype\;|1\;tcp_mbtype\;|g;s|tcp_mbreg\;|confTemp\;1\;tcp_mbreg\;|g;s|\;iotype\;| Alarm\;iotype\;|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        number=$((number + 1))
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU_|conf_$OBJECT\_|g;s|\;MPU\;|\;$OBJECT\;|g;s|_S|_Warn_S|g;s|_AS|_Warn_AS|g;s|fqcTemp\;1\;winFQCTemp\;1\;||g;s|\;tcp_mbtype\;|2\;tcp_mbtype\;|g;s|tcp_mbreg\;|confTemp\;1\;tcp_mbreg\;|g;s|\;iotype\;| Warn\;iotype\;|g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        fi
}

gen_cw()
{
        if  echo $line | grep -q $conf 
        then
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU_|w_conf_$OBJECT\_|g;s|\;MPU\;|\;$OBJECT\;|g;s|from|to|g;s|0x04|0x10|g;s|_S|_Alarm_S|g;s|_AS|_Alarm_AS|g;s|\;tcp_mbtype\;|3\;tcp_mbtype\;|g;s|fqcTemp\;1\;winFQCTemp\;1\;||g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        number=$((number + 1))
        echo $line | sed "s|id;|		<item id=\"$number|g;s|<!--|		<!--|g;s|MPU_|w_conf_$OBJECT\_|g;s|\;MPU\;|\;$OBJECT\;|g;s|from|to|g;s|0x04|0x10|g;s|_S|_Warn_S|g;s|_AS|_Warn_AS|g;s|\;tcp_mbtype\;|4\;tcp_mbtype\;|g;s|fqcTemp\;1\;winFQCTemp\;1\;||g;${OBJ_RULSE}${rules}${OBJ_RULSE2}"
        fi
}



for OBJ in "MPU1" "MPU1_c" "w_MPU1" "w_MPU1_c"; do

	echo "		<!-- Датчики "$OBJ" (сгенерировано из csv) $USER: `date -u +"%d.%m.%Y"` -->"

	#Иногда требуется то же название, но прописными буквами
	OBJl=$(echo $OBJ | tr '[:upper:]' '[:lower:]')

    case "$OBJ" in
    "MPU1" )
        OBJ_RULSE="s|fqcTemp|fqcTemp1|g;s|lsuTemp|lsuTemp1|g;s|driveTemp1|drivewinTemp1|g;s|driveTemp2|drivebeaTemp3|g;s|tTemp1|transTemp1|g;s|tTemp2|transTemp3|g"
        OBJ_RULSE2=";s|DDD|1|g;"
        ;;
    esac

	#Цикл чтения из csv
	while read line; do
		
    case "$OBJ" in
    "MPU1" )
            gen
        echo "MPU1 $number"  >> 1
        ;;
    "MPU1_c" )
	    gen_c
        echo "MPU1_c $number"  >> 1
	;;
    "w_MPU1" )
	    gen_w
        echo "w_MPU1 $number"  >> 1
        ;;
    "w_MPU1_c" )
	    gen_cw
        echo "w_MPU1_c $number"  >> 1
    ;;
    esac
		#Увеличение id
		number=$((number + 1))
	done < $input
	
	case "$OBJ" in
    "MPU1" )
	    let number=$((inc_numconf))
        ;;
	"MPU1_c" )
	    let number=$((inc_number + 1))
        ;;
    "w_MPU1" )
	    let number=$((inc_number+inc_numconf))
        ;;
	"w_MPU1_c" )
	    let number=$((inc_number+inc_numconf))
        ;;
    esac

	
	shift_number=$number

done
