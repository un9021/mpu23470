#!/bin/bash

#Скрипт-болванка для создания ..._make.sh

. make_common.sh

[ -z "$correctlist" ] && correctlist=common_rules

input=$1
#Номер, с которого начнется отсчет id
number=$2
[ -z "$number" ] && number=300
#Сдвиговый параметр, на сколько будет отступ между датчиками (например, между TV1 и TV2)
inc_number=5000

shift_number=$number

PROG="${0##*/}"

print_help()
{
		[ "$1" = 0 ] || exec >&2
		cat <<EOF
		Usage: $PROG name_file [BEGIN_ID]
		name_file - input file(*.csv with separator ";")
		BEGIN_ID - first id item. Default: $number
EOF
    [ -n "$1" ] && exit "$1" || exit
}

[[ -z "$1" ]] && print_help 1

#Генерируем правила замен и common_rules
rules=$(get_autozamena_sed_rules "$correctlist")

for OBJ in "MPU1"; do

	echo "		<!-- Датчики "$OBJ" (сгенерировано из csv) $USER: `date -u +"%d.%m.%Y"` -->"

	#Иногда требуется то же название, но прописными буквами
	OBJl=$(echo $OBJ | tr '[:upper:]' '[:lower:]')

	#Сюда можно вставить проверки и правки правил замен, непосредственно перед чтением csv-файла
#####################################################
#                                                   #
#    case "$OBJ" in                                 #
#    "OBJ1" )                                       #
#           OBJ_RULES=";s|...|...|g;"               #
#           ;;                                      #
#    "OBJ2" )                                       #
#           OBJ_RULES=                              #
#           ;;                                      #
#    "OBJ3" )                                       #
#           ...                                     #
#           ...                                     #
#           ;;                                      #
#    esac                                           #
#                                                   #
#####################################################


	#Цикл чтения из csv
	while read line; do

		echo $line | sed "s|id;|		<item id=\"$number|g;$OBJ_RULSE${rules}"

		#Увеличение id
		number=$((number+1))
	done < $input

	let number=$((shift_number+inc_number))
	shift_number=$number

done
