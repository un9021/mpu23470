#!/bin/sh
#
# Объёденение кажой пары строк
# формирование специальной команды для создания ссылок вида C:XXX на EXXX

PROG="${0##*/}"
FMT="dec"

print_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG file

$PROG - ask-file generator
xmlfile - source xml-file. Default valuse is 'configure.xml'

Valid options are:
  -h, --help	display help screen
  -o, --output  print command to output
  -x, --xml		генерировать xmlfile
  -d, --dec     generate ID in decimal format. Default.
  -z, --hex     generate ID in hex format.
  
EOF
    [ -n "$1" ] && exit "$1" || exit
}

output=
XML=


#parse command line options
TEMP=`getopt -n $PROG -o o,h,x,d,z -l output,help,xml,hex,dec -- "$@"` || exit 1
eval set -- "$TEMP"

while :; do
    case "$1" in
	-h|--help) print_usage 0
	    ;;
	-o|--output)
		output=1
		;;
	-x|--xml)
		XML=1
		;;
	-d|--dec)
		FMT="dec"
		;;
	-z|--hex)
		FMT="hex"
		;;
		
	--) shift; break
	;;
	*) "unrecognized option: $1"
	exit 1
	;;
    esac
    shift
done

sfile="$1"
[ -z "$sfile" ] && print_usage 0

second=
ok=
err=
reg=

if [ -n "$XML" ]; then 
	echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>";
	echo "<elist>"
fi

REG=
BIT=
function split_reg()
{
	REG=$1
	BIT=$2
}

for s in `cat $sfile`; do
	if [ -z "$second" ]; then
		err=`echo -n $s` && second=1 && ok=
	else
		reg=`echo -n " $s" && echo ""` && second= && ok=1
	fi
	
	if [ -n "$ok" ]; then
		ok=
		reg=`echo $reg | sed -e "s|[с,c,С]|C|i" | sed -e "s|:|.|i"`
		err=`echo $err | sed -e "s|[Е,E]|e|i"`

		if [ "$FMT" == "dec" ]; then
			# split
			arr=$(echo $reg | tr "." "\n")
			split_reg ${arr}
			reg_val=$REG
			reg_bit=$BIT
		
			# to dec
			reg_val=$(printf %d 0x$REG)
			# join
			reg="$reg_val.$reg_bit"
    	fi

		if [ -z "$XML" ]; then 
		
			if [ -n "$output" ]; then 
				echo "ln -s $err $reg"
			else
				ln -sf "$err" $reg
			fi
		else
			echo "	<item name=\"$err\" id=\"$reg\" />"
		fi
	fi
done

if [ -n "$XML" ]; then 
	echo "</elist>"
fi
