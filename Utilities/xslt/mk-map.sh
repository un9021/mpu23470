#!/bin/sh
top_builddir=

xmlfile=/conf/configure.xml

IDLIST=1
NM=
WL=
PROG=mk-map.sh

print_usage()
{
    [ "$1" = 0 ] || exec >&2
    cat <<EOF
Usage: $PROG [options] [xmlfile]

$PROG 	- modbusmap include file generator
xmlfile - source xml-file. Default valuse is 'configure.xml'

Valid options are:
  -h, --help	display help screen
  -i, --id - generate ID list. Default
  -t, --name  - generate name list
  -w, --write  - generate write list (direct=to)
EOF
    [ -n "$1" ] && exit "$1" || exit
}

#parse command line options
TEMP=`getopt -n $PROG -o h,i,t,w -l help,id,name,write -- "$@"` || exit 1
eval set -- "$TEMP"

while :; do
    case "$1" in
	-h|--help) print_usage 0
	    ;;

	-i|--id)
		IDLIST=1
		break;
		;;

	-t|--name)
		NM=1
		break;
		;;

	-w|--write)
		WL=1
		break;
		;;

	--) shift; break
	;;

	*) "unrecognized option: $1"
	exit 1
	;;
    esac
    shift
done

fname=$( basename $xmlfile )

[ -n "$IDLIST" ] && xslfile=/Utilities/xslt/idList.xsl
[ -n "$NM" ] && xslfile=/Utilities/xslt/nameList.xsl
[ -n "$WL" ] && xslfile=/Utilities/xslt/writeList.xsl


xsltproc --stringparam FILENAME "" \
	--stringparam SYS "" $xslfile $xmlfile | grep -v '^[[:space:]]\{0,\}$' | sort -n
